import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  cart:any
  total:any = 0
  dummyCart : any;
  constructor(){
    this.cart = [];
    this.dummyCart = [];
  }
  title = 'Restaurant';
  
  items = [
    {
      'name' : 'Product 1',
      'id' : 1,
      'price' : 200
    },
    {
      'name' : 'Product 2',
      'id' : 2,
      'price' : 100,
      'addon': [
        {
          'id' : 123,
          'name' :'Cheese Slice',
          'price' : 20
        },
        {
          'id' : 555,
          'name' :'Choco chips',
          'price' : 15
        },
        {
          'id' : 666,
          'name' :'Panner',
          'price' : 10
        }
      ]
      
      
    },
    {
      'name' : 'Product 3',
      'id' : 3,
      'price' : 500,
      'addon': [
        {
          'id' : 22211,
          'name' :'Cheese 1',
          'price' : 20
        },
        {
          'id' : 55115,
          'name' :'Choco 2',
          'price' : 15
        },
        {
          'id' : 1111111,
          'name' :'Cheese 4',
          'price' : 10
        }
      ]
    },
    {
      'name' : 'Product 4',
      'id' : 4,
      'price' : 2200
    }
  ]

  addToMyCart(item){


    if(item.addon){
     
  

    }
    // console.log("Item - ", item);
    if(this.cart.length == 0){
      item.count = 1;
      this.cart.push(item);
    }else{
      var repeat = false;
      for(var i = 0; i< this.cart.length; i++){
        if(this.cart[i].id === item.id){
          repeat = true;
          this.cart[i].count +=1;
        }
      }
      if (!repeat) {
        item.count = 1;
         this.cart.push(item);	
    }
    }

    this.total += parseFloat(item.price);
  }

  removeItem(item){
    // console.log(item);
    
    if(item.count >=  1){
      item.count -= 1;

      if(item.count === 0){
        var index = this.cart.indexOf(item);
        this.cart.splice(index, 1)
      }
    this.total -= parseFloat(item.price);

    
    
    }

  }

  purchaseProducts(){
    // console.log(this.cart);
    
  }

  addonAdd(parentItem, addonItem, event){
    if(event.target.checked){
      // console.log("Parent Item: ", parentItem);
      // console.log("Addon Item: ", addonItem);
      this.cart['parentItem.id'].push(addonItem);

      console.log("Updated Cart", this.cart);
      
    }else{

    }
  
  
  }

}
